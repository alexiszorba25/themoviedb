# TheMovieDB

App demo en Android accediendo a la API de The Movie DataBase (https://developers.themoviedb.org) para visualizar listado de pel�culas y acceder al detalle.
Se utiliz� Retrofit y patr�n MVC y MVP

# Api Key

Para testear la aplicaci�n deben obtener una Api Key e ingresarla en el string "api_key"

# Arquitectura desarrollada

Se desarroll� sobre distintas capas que se detallan a continuaci�n:

## Datos y Persistencia
Por un lado se encuentran las clases de objetos de datos propiamente dichos:


**Movie** Pel�cula recibida desde la Api de TMDB.


**LocalMovie** Similar a la anterior pero utilizada como tabla de almacenamiento local en SQLite con Cupboard. Tiene un atributo adicional "mCategory" que indica la categor�a de la pel�cula "Popular", "Top Rated" y "Upcoming".

**TmdbApiResponse** Clase creada por la estructura que se recibe del servicio de TMDB que contiene informaci�n adicional a las pel�culas como ser la p�gina, etc.
Por otro lado se encuentran las clases que acceden a los datos y almacenan ya sea en Memoria (Lista), Api (s�lo acceso) y Local (Sqlite):

**MemoryMoviesDataSource**,**LocalMoviesDataSource** y **CloudMoviesDataSource** cada una con su correspondiente Interfaz de implementaci�n.

**MoviesRepository** con su correspondiente interfaz que administra el acceso a las distintas fuentes de datos seg�n la situaci�n.

## Negocio
Las distintas clases que administran tanto el acceso a las API REST como a la DB SQLite: **RestService**, **ServiceHelper**, **ErrorResponse**, **TmdbDeserializer** y **TmdbTestDatabaseHelper**

**DependencyProvider** que orquesta la generaci�n y uso de los distintos repositorios de datos.

**MoviesMvpController** que crea los Presenter de acuerdo a la categor�a de la pel�cula.

## Presentaci�n
**MoviesPresenter** y **MovieDetailPresenter** que se encargan de preparar los datos recibidos de las fuentes para enviar a los fragments. En el caso del MovieDetailPresenter directamente recibe el objeto Movie al clickear sobre una pel�cula y lo muestra sin acceder remotamente a la informaci�n.

**MoviesAdapter** es la clase encargada de manejar el listado de pel�culas para mostrar en los fragments.

**PopularFragment**, **TopFragment** y **UpcomingFragment** son las 3 solapas que muestran el listado de cada tipo de pel�cula.

**MovieDetailActivity** y **MovieDetailFragment** muestran el detalle de la pel�cula.

**MainActivity** es la activity principal que contiene el ButtonNavigationView con los 3 fragmentos.

# Principio de Responsabilidad Unica y C�digo Limpio
Es uno de los principios de SOLID para escribir c�digo que sea m�s legible, mantenible, escalable y f�cil de testear. Est� tambi�n relacionado con las caracter�sticas de POO de Alta Cohesi�n y Bajo Acomplamiento. Una clase de hacer s�lo la tarea para la cual fue creada. Cuanto m�s funcionalidades agregamos, m�s se dificulta su mantenci�n y testeo.

Un buen c�digo o c�digo limpio a mi entender es cuando es legible y entendible para otro programador incluso aunque no conozca el lenguaje en el que est� programado e interprete su funci�n y no se necesite comentar el c�digo. Por ejemplo, los nombres de los m�todos/objetos tienen que ser claros y tienen que usarse pocos par�metros.


