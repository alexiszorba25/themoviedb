package com.spiraquis.alexis.imdb_test.external.api;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.spiraquis.alexis.imdb_test.R;
import com.spiraquis.alexis.imdb_test.application.TmdbApplication;
import com.spiraquis.alexis.imdb_test.movies.domain.model.TmdbApiResponse;
import com.spiraquis.alexis.imdb_test.movies.domain.model.Movie;

import java.util.Map;
import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ServiceHelper {

    private static OkHttpClient httpClient = new OkHttpClient();
    private static ServiceHelper instance = new ServiceHelper();
    private RestService service;

    private ServiceHelper() {

        Retrofit retrofit = createAdapter().build();
        service = retrofit.create(RestService.class);
    }
    public static ServiceHelper getInstance() {
        return instance;
    }

    private Retrofit.Builder createAdapter() {
        Gson gson = new GsonBuilder()
                .setLenient()
                .registerTypeAdapter(Movie.class, new TmdbDeserializer())
                .create();
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.NONE);
        httpClient.newBuilder().addInterceptor(interceptor)
                .retryOnConnectionFailure(true)
                .connectTimeout(15, TimeUnit.SECONDS)
                .build();

        return new Retrofit.Builder()
                .baseUrl(TmdbApplication.getContext().getResources().getString(R.string.base_url))
                .client(httpClient)
                .addConverterFactory(GsonConverterFactory.create(gson));
    }
    public Call<TmdbApiResponse> getPopular(Map<String, String> params){
        return service.getPopular(params);
    }
    public Call<TmdbApiResponse> getTop(Map<String, String> params){
        return service.getTop(params);
    }
    public Call<TmdbApiResponse> getUpcoming(Map<String, String> params){
        return service.getUpcoming(params);
    }
    public Call<TmdbApiResponse> getMovieDetail(int idMovie, Map<String, String> params){
        return service.getMovieDetail(idMovie, params);
    }
}