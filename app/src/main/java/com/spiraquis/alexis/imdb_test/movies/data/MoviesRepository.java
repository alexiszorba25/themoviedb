package com.spiraquis.alexis.imdb_test.movies.data;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.annotation.NonNull;

import com.spiraquis.alexis.imdb_test.R;
import com.spiraquis.alexis.imdb_test.application.TmdbApplication;
import com.spiraquis.alexis.imdb_test.selection.Query;
import com.spiraquis.alexis.imdb_test.movies.data.datasource.cloud.ICloudMoviesDataSource;
import com.spiraquis.alexis.imdb_test.movies.data.datasource.local.ILocalMoviesDataSource;
import com.spiraquis.alexis.imdb_test.movies.data.datasource.memory.IMemoryMoviesDataSource;
import com.spiraquis.alexis.imdb_test.movies.domain.model.Movie;

import java.util.List;
import java.util.Map;
import java.util.Objects;

import static com.spiraquis.alexis.imdb_test.application.TmdbApplication.POPULAR;
import static com.spiraquis.alexis.imdb_test.application.TmdbApplication.TOP;
import static com.spiraquis.alexis.imdb_test.application.TmdbApplication.UPCOMING;

/**
 * Repositorio de películas
 */
public class MoviesRepository implements IMoviesRepository {

    private final IMemoryMoviesDataSource mMemoryMoviesDataSource;
    private final ILocalMoviesDataSource mLocalMoviesDataSource;
    private final ICloudMoviesDataSource mCloudMoviesDataSource;

    private final ConnectivityManager mConnectivityManager;

    private boolean mReload = false;


    public MoviesRepository(@NonNull IMemoryMoviesDataSource memoryDataSource,
                             @NonNull ILocalMoviesDataSource localMoviesDataSource,
                             @NonNull ICloudMoviesDataSource cloudDataSource,
                             Context context) {
        mMemoryMoviesDataSource = Objects.requireNonNull(memoryDataSource);
        mLocalMoviesDataSource = Objects.requireNonNull(localMoviesDataSource);
        mCloudMoviesDataSource = Objects.requireNonNull(cloudDataSource);
        mConnectivityManager = (ConnectivityManager)
                context.getSystemService(Context.CONNECTIVITY_SERVICE);

    }

    @Override
    public void getMovies(@NonNull final Query query, int category, final Map<String, String> params, final getMoviesCallback callback) {
        Objects.requireNonNull(query, "query no puede ser null");

        if (!mMemoryMoviesDataSource.mapIsNull() && !mReload && mMemoryMoviesDataSource.find(query).size()>0) {
            callback.onMoviesLoaded(mMemoryMoviesDataSource.find(query));
        }else {
            getMoviesFromServer(query, category, params, callback);
        }
    }
    private void getMoviesFromLocal(final Query query, int category, final Map<String, String> params, final getMoviesCallback callback) {
        mLocalMoviesDataSource.get(query, category, new ILocalMoviesDataSource.GetCallback() {
            @Override
            public void onMoviesLoaded(List<Movie> movies) {
                refreshMemoryDataSource(movies, mReload);
                callback.onMoviesLoaded(mMemoryMoviesDataSource.find(query));
            }

            @Override
            public void onDataNotAvailable(String error) {
                callback.onDataNotAvailable(TmdbApplication.getContext().getString(R.string.error_no_network));
            }
        });
    }
    private void getMoviesFromServer(final Query query, int category, Map<String, String> params, final getMoviesCallback callback) {

        if (!isOnline()) {
            getMoviesFromLocal(query, category, params, callback);
            return;
        }
        switch (category) {
            case POPULAR:
                mCloudMoviesDataSource.getPopular(
                        query, params, new ICloudMoviesDataSource.MovieServiceCallback() {
                            @Override
                            public void onLoaded(List<Movie> movies) {
                                refreshMemoryDataSource(movies, mReload);
                                refreshLocalDataSource(movies, POPULAR, mReload);
                                callback.onMoviesLoaded(mMemoryMoviesDataSource.find(query));
                            }

                            @Override
                            public void onError(String error) {
                                callback.onDataNotAvailable(error);
                            }
                        }
                );
                break;
            case TOP:
                mCloudMoviesDataSource.getTop(
                        query, params, new ICloudMoviesDataSource.MovieServiceCallback() {
                            @Override
                            public void onLoaded(List<Movie> movies) {
                                refreshMemoryDataSource(movies, mReload);
                                refreshLocalDataSource(movies, TOP, mReload);
                                callback.onMoviesLoaded(mMemoryMoviesDataSource.find(query));
                            }

                            @Override
                            public void onError(String error) {
                                callback.onDataNotAvailable(error);
                            }
                        }
                );
                break;
            case UPCOMING:
                mCloudMoviesDataSource.getUpcoming(
                        query, params, new ICloudMoviesDataSource.MovieServiceCallback() {
                            @Override
                            public void onLoaded(List<Movie> movies) {
                                refreshMemoryDataSource(movies, mReload);
                                refreshLocalDataSource(movies, UPCOMING, mReload);
                                callback.onMoviesLoaded(mMemoryMoviesDataSource.find(query));
                            }

                            @Override
                            public void onError(String error) {
                                callback.onDataNotAvailable(error);
                            }
                        }
                );
        }
    }

    private void refreshMemoryDataSource(List<Movie> movies, boolean reload) {
        if (reload) {
            mMemoryMoviesDataSource.deleteAll();
        }
        for (Movie movie : movies) {
           mMemoryMoviesDataSource.save(movie);
        }
        mReload = false;
    }

    private void refreshLocalDataSource(List<Movie> movies, int category, boolean reload) {
        if (reload) {
            mLocalMoviesDataSource.delete(category);
        }
        for (Movie movie : movies) {
            mLocalMoviesDataSource.save(movie, category);
        }
        mReload = false;
    }

    private boolean isOnline() {
        NetworkInfo info = mConnectivityManager.getActiveNetworkInfo();
        return info != null && info.isConnectedOrConnecting();
    }

    @Override
    public void refreshMovies() {
        mReload = true;
    }

}
