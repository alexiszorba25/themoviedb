package com.spiraquis.alexis.imdb_test.moviedetail.presentation;

/**
 * Contrato MVP para el detalle de la película.
 */

public interface MovieDetailMvp {
    interface View {
        void showPoster(String imageUrl);
        void showBackPoster(String imageUrl);
        void showTitle(String name);
        void showVoteCount(int voteCount);
        void showVoteAverage(float voteAverage);
        void showReleaseDate(String releaseDate);
        void showOverview(String overview);
        void setPresenter(Presenter movieDetailPresenter);
    }

    interface Presenter{
        void showMovieDetail();
    }
}
