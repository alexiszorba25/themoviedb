package com.spiraquis.alexis.imdb_test.selection.selector;

import java.util.List;

/**
 * Abstracción para selectores de listas Java
 */

public interface FilterSelector<T, S> extends Selector {
    List<T> filterListRows(List<T> items, S filter);
}