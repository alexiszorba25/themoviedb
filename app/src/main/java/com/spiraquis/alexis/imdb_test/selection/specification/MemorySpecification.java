package com.spiraquis.alexis.imdb_test.selection.specification;

/**
 * Patrón de especificación para las películas
 */
public interface MemorySpecification<T> extends Specification{
    boolean isSatisfiedBy(T item);
}
