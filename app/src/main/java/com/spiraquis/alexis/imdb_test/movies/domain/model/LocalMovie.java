package com.spiraquis.alexis.imdb_test.movies.domain.model;

public class LocalMovie {
    private Long _id; // for cupboard
    private int mId;
    private int mCategory;
    private String mTitle;
    private String mOrigLanguage;
    private String mPosterPath;
    private String mOverview;
    private String mReleaseDate;
    private String mBackdropPath;
    private int mVoteCount;
    private float mVoteAverage;

    public LocalMovie(int mId, int mCategory, String mTitle, String mOrigLanguage, String mPosterPath, String mOverview, String mReleaseDate, String mBackdropPath, int mVoteCount, float mVoteAverage) {
        this.mId = mId;
        this.mCategory = mCategory;
        this.mTitle = mTitle;
        this.mOrigLanguage = mOrigLanguage;
        this.mPosterPath = mPosterPath;
        this.mOverview = mOverview;
        this.mReleaseDate = mReleaseDate;
        this.mBackdropPath = mBackdropPath;
        this.mVoteCount = mVoteCount;
        this.mVoteAverage = mVoteAverage;
    }
    public LocalMovie(){}
    public Long get_id() {
        return _id;
    }

    public int getmId() {
        return mId;
    }

    public int getmCategory() {
        return mCategory;
    }

    public String getmTitle() {
        return mTitle;
    }

    public String getmOrigLanguage() {
        return mOrigLanguage;
    }

    public String getmPosterPath() {
        return mPosterPath;
    }


    public String getmOverview() {
        return mOverview;
    }

    public String getmReleaseDate() {
        return mReleaseDate;
    }

    public String getmBackdropPath() {
        return mBackdropPath;
    }

    public int getmVoteCount() {
        return mVoteCount;
    }

    public float getmVoteAverage() {
        return mVoteAverage;
    }
}
