package com.spiraquis.alexis.imdb_test.moviedetail.presentation;

import com.spiraquis.alexis.imdb_test.R;
import com.spiraquis.alexis.imdb_test.movies.domain.model.Movie;
import com.spiraquis.alexis.imdb_test.movies.domain.usecase.IGetMovies;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import static com.spiraquis.alexis.imdb_test.application.TmdbApplication.getContext;

/**
 * Implementación concreta del presentador de detalle de la película.
 */

public class MovieDetailPresenter implements MovieDetailMvp.Presenter {

    private int mIdMovie;
    private Movie movie;

    // Relaciones
    private MovieDetailMvp.View mMovieDetailView;


    public MovieDetailPresenter(int idMovie, MovieDetailMvp.View movieDetailView,
                                IGetMovies getMovies) {
        mIdMovie = idMovie;
        mMovieDetailView = Objects.requireNonNull(movieDetailView, "movieDetailView no puede ser null");
        IGetMovies mgetMovies = Objects.requireNonNull(getMovies, "GetMovies no puede ser null");
    }
    public MovieDetailPresenter(Movie movie, MovieDetailMvp.View movieDetailView) {
        this.movie = movie;
        mMovieDetailView = Objects.requireNonNull(movieDetailView, "movieDetailView no puede ser null");
    }
    public int getIdMovie() {
        return mIdMovie;
    }

    private static Map<String, String> setWsParams() {
        Map<String, String> wsParams = new HashMap<>();
        wsParams.put("api_key", getContext().getString(R.string.api_key));
        return wsParams;
    }
    @Override
    public void showMovieDetail() {
        mMovieDetailView.showPoster(movie.getmPosterPath());
        mMovieDetailView.showBackPoster(movie.getmBackdropPath());
        mMovieDetailView.showTitle(movie.getmTitle());
        mMovieDetailView.showVoteAverage(movie.getmVoteAverage());
        mMovieDetailView.showVoteCount(movie.getmVoteCount());
        mMovieDetailView.showReleaseDate(movie.getmReleaseDate());
        mMovieDetailView.showOverview(movie.getmOverview());
    }
}
