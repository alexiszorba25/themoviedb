package com.spiraquis.alexis.imdb_test.application;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.support.multidex.MultiDex;
import android.support.multidex.MultiDexApplication;

import com.spiraquis.alexis.imdb_test.external.sqlite.TmdbTestDatabaseHelper;

public class TmdbApplication extends MultiDexApplication{

    private static Context mContext;
    public static TmdbTestDatabaseHelper dbHelper;
    public static SQLiteDatabase db;
    public static final int POPULAR = 1;
    public static final int TOP = 2;
    public static final int UPCOMING = 3;

    @Override
    public void onCreate() {
        super.onCreate();
        mContext = this;
        // setup local database for movies
        dbHelper = new TmdbTestDatabaseHelper(this);
        db = dbHelper.getWritableDatabase();
    }
    public static Context getContext(){
        return mContext;
    }
    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(newBase);
        MultiDex.install(this);
    }
}
