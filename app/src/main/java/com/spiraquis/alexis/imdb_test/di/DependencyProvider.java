package com.spiraquis.alexis.imdb_test.di;

import android.content.Context;
import android.support.annotation.NonNull;

import com.spiraquis.alexis.imdb_test.movies.data.MoviesRepository;
import com.spiraquis.alexis.imdb_test.movies.data.datasource.cloud.CloudMoviesDataSource;
import com.spiraquis.alexis.imdb_test.movies.data.datasource.local.LocalMoviesDataSource;
import com.spiraquis.alexis.imdb_test.movies.data.datasource.memory.MemoryMoviesDataSource;
import com.spiraquis.alexis.imdb_test.movies.domain.usecase.GetMovies;

import java.util.Objects;

import static com.spiraquis.alexis.imdb_test.application.TmdbApplication.POPULAR;
import static com.spiraquis.alexis.imdb_test.application.TmdbApplication.TOP;
import static com.spiraquis.alexis.imdb_test.application.TmdbApplication.UPCOMING;

/**
 * Contenedor de dependencias
 */
public final class DependencyProvider {
    private static Context mContext;
    private static MoviesRepository mPopularRepository;
    private static MoviesRepository mTopRepository;
    private static MoviesRepository mUpcomingRepository;
    private static MemoryMoviesDataSource memoryPopularSource;
    private static MemoryMoviesDataSource memoryTopSource;
    private static MemoryMoviesDataSource memoryUpcomingSource;
    private static CloudMoviesDataSource cloudPopularSource;
    private static CloudMoviesDataSource cloudTopSource;
    private static CloudMoviesDataSource cloudUpcomingSource;
    private static LocalMoviesDataSource localPopularSource;
    private static LocalMoviesDataSource localTopSource;
    private static LocalMoviesDataSource localUpcomingSource;

    private DependencyProvider() {
    }

    private static MoviesRepository providePopularRepository(@NonNull Context context) {
        mContext = Objects.requireNonNull(context);
        if (mPopularRepository == null) {
            mPopularRepository = new MoviesRepository(Objects.requireNonNull(getMemorySource(POPULAR)), Objects.requireNonNull(getLocalSource(POPULAR)),
                    Objects.requireNonNull(getCloudSource(POPULAR)), context);
        }
        return mPopularRepository;
    }

    private static MoviesRepository provideTopRepository(@NonNull Context context) {
        mContext = Objects.requireNonNull(context);
        if (mTopRepository == null) {
            mTopRepository = new MoviesRepository(Objects.requireNonNull(getMemorySource(TOP)), Objects.requireNonNull(getLocalSource(TOP)),
                    Objects.requireNonNull(getCloudSource(TOP)), context);
        }
        return mTopRepository;
    }
    private static MoviesRepository provideUpcomingRepository(@NonNull Context context) {
        mContext = Objects.requireNonNull(context);
        if (mUpcomingRepository == null) {
            mUpcomingRepository = new MoviesRepository(Objects.requireNonNull(getMemorySource(UPCOMING)), Objects.requireNonNull(getLocalSource(UPCOMING)),
                    Objects.requireNonNull(getCloudSource(UPCOMING)), context);
        }
        return mUpcomingRepository;
    }
    private static MemoryMoviesDataSource getMemorySource(int category) {
        switch (category) {
            case POPULAR:
                if (memoryPopularSource == null) {
                    memoryPopularSource = new MemoryMoviesDataSource();
                }
                return memoryPopularSource;
            case TOP:
                if (memoryTopSource == null) {
                    memoryTopSource = new MemoryMoviesDataSource();
                }
                return memoryTopSource;
            case UPCOMING:
                if (memoryUpcomingSource == null) {
                    memoryUpcomingSource = new MemoryMoviesDataSource();
                }
                return memoryUpcomingSource;
            default:
                return null;
        }
    }

    private static CloudMoviesDataSource getCloudSource(int category) {
        switch (category) {
            case POPULAR:
                if (cloudPopularSource == null) {
                    cloudPopularSource = new CloudMoviesDataSource();
                }
                return cloudPopularSource;
            case TOP:
                if (cloudTopSource == null) {
                    cloudTopSource = new CloudMoviesDataSource();
                }
                return cloudTopSource;
            case UPCOMING:
                if (cloudUpcomingSource == null) {
                    cloudUpcomingSource = new CloudMoviesDataSource();
                }
                return cloudUpcomingSource;
            default:
                return null;
        }
    }
    private static LocalMoviesDataSource getLocalSource(int category) {
        switch (category) {
            case POPULAR:
                if (localPopularSource == null) {
                    localPopularSource = new LocalMoviesDataSource(mContext.getContentResolver());
                }
                return localPopularSource;
            case TOP:
                if (localTopSource == null) {
                    localTopSource = new LocalMoviesDataSource(mContext.getContentResolver());
                }
                return localTopSource;
            case UPCOMING:
                if (localUpcomingSource == null) {
                    localUpcomingSource = new LocalMoviesDataSource(mContext.getContentResolver());
                }
                return localUpcomingSource;
            default:
                return null;
        }
    }
    public static GetMovies providegetPopular(@NonNull Context context) {
        return new GetMovies(providePopularRepository(context));
    }
    public static GetMovies providegetTop(@NonNull Context context) {
        return new GetMovies(provideTopRepository(context));
    }
    public static GetMovies providegetUpcoming(@NonNull Context context) {
        return new GetMovies(provideUpcomingRepository(context));
    }
}
