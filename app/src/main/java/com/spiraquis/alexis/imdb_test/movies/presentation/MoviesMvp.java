package com.spiraquis.alexis.imdb_test.movies.presentation;

import android.widget.ImageView;

import com.spiraquis.alexis.imdb_test.movies.domain.model.Movie;

import java.util.List;
import java.util.Map;

/**
 * Contrato MVP para la lista de películas
 */
public interface MoviesMvp {
    interface View {
        void showMovies(List<Movie> movies);

        void showLoadingState(boolean show);

        void showEmptyState();

        void showMoviesError(String msg);

        void showMoviesPage(List<Movie> movies);

        void showLoadMoreIndicator(boolean show);

        void allowMoreData(boolean show);

        void showMovieDetailScreen(Movie movie, ImageView imagepath);

        void setPresenter(Presenter presenter);

        Presenter getPresenter();
    }

    interface Presenter {
        void loadMovies(boolean reload, int category, Map<String, String> params);

        void openMovieDetails(Movie movie, ImageView imagepath);

    }
}
