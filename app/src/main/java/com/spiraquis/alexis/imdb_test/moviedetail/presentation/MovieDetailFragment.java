package com.spiraquis.alexis.imdb_test.moviedetail.presentation;


import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.spiraquis.alexis.imdb_test.R;
import com.spiraquis.alexis.imdb_test.util.GlideApp;

import java.util.Objects;

import static com.spiraquis.alexis.imdb_test.moviedetail.MovieDetailActivity.EXTRA_ID_MOVIE;

/**
 * Fragmento que representa la implementación concreta de la vista del detalle de la película
 */
public class MovieDetailFragment extends Fragment
        implements MovieDetailMvp.View {

    // Miembros UI
    private ImageView mBackImageMovie;
    private ImageView mPosterPath;
    private TextView mOverview;
    private TextView mVoteAverage;
    private TextView mVoteCount;
    private TextView mReleaseDate;
    private CollapsingToolbarLayout collapser;
    int idMovie;
    // Miembros composición
    private MovieDetailMvp.Presenter mMovieDetailPresenter;

    public static MovieDetailFragment newInstance() {
        return new MovieDetailFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if (container == null) {
            return null;
        }

        View root = inflater.inflate(R.layout.fragment_movie_detail, container, false);
        setToolbar(root);

        View mDetailContainer = root.findViewById(R.id.container_movie_detail_views);
        mBackImageMovie = root.findViewById(R.id.image_movie);
        mPosterPath = root.findViewById(R.id.posterpath);
        mVoteAverage = root.findViewById(R.id.vote_average);
        mVoteCount = root.findViewById(R.id.vote_count);
        mReleaseDate = root.findViewById(R.id.release_date);
        mOverview = root.findViewById(R.id.overview);
        ProgressBar mProgressView = root.findViewById(R.id.progress_indicator);
        collapser = root.findViewById(R.id.toolbar_layout);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            String imageTransitionName = getActivity().getIntent().getStringExtra("transition");
            mPosterPath.setTransitionName(imageTransitionName);
        }
        idMovie = getActivity().getIntent().getIntExtra(EXTRA_ID_MOVIE,0);
        return root;
    }

    private void setToolbar(View root) {
        Toolbar toolbar = root.findViewById(R.id.toolbar);
        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);
        ActionBar ab = ((AppCompatActivity) getActivity()).getSupportActionBar();
        ab.setDisplayHomeAsUpEnabled(true);
        ab.setDisplayShowHomeEnabled(true);
        ab.setTitle(null);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (getView() != null) {
            mMovieDetailPresenter.showMovieDetail();
        }
    }

    @Override
    public void showPoster(String imageUrl) {
        GlideApp.with(this)
                .load(getContext().getString(R.string.base_url_poster)+imageUrl)
                .into(mPosterPath);
    }

    @Override
    public void showBackPoster(String imageUrl) {
        GlideApp.with(this)
                .load(getContext().getString(R.string.base_url_backposter)+imageUrl)
                .into(mBackImageMovie);
    }

    @Override
    public void showTitle(String name) {
        collapser.setTitle(name);
    }

    @Override
    public void showVoteCount(int voteCount) {
        mVoteCount.setText(getString(R.string.txt_vote_count,voteCount));
    }

    @Override
    public void showVoteAverage(float voteAverage) {
        mVoteAverage.setText(String.valueOf(voteAverage));
    }

    @Override
    public void showReleaseDate(String releaseDate) {
        mReleaseDate.setText(releaseDate);
    }

    @Override
    public void showOverview(String description) {
        mOverview.setText(description);
    }

    @Override
    public void setPresenter(MovieDetailMvp.Presenter movieDetailPresenter) {
        mMovieDetailPresenter = Objects.requireNonNull(movieDetailPresenter);
    }
    @Override
    public void onCreateOptionsMenu(final Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_movie_detail, menu);
        super.onCreateOptionsMenu(menu,inflater);
    }
        @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_share) {
            Intent i = new Intent(Intent.ACTION_SEND);
            i.setType("text/plain");
            i.putExtra(Intent.EXTRA_SUBJECT, getResources().getString(R.string.txt_share));
            i.putExtra(Intent.EXTRA_TEXT, getResources().getString(R.string.base_url_share) + idMovie);
            startActivity(Intent.createChooser(i, getResources().getString(R.string.txt_share_title)));
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
