package com.spiraquis.alexis.imdb_test.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class DateUtils {
    public static String getYearFromDate(String date) {
        SimpleDateFormat formatterInput = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
        try {
            Date formatted = formatterInput.parse(date);
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(formatted);
            return ((Integer)calendar.get(Calendar.YEAR)).toString();
        }catch(ParseException e) {
            return "";
        }
    }
}
