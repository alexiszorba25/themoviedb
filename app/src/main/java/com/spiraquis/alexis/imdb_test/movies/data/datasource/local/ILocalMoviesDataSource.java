package com.spiraquis.alexis.imdb_test.movies.data.datasource.local;

import android.support.annotation.NonNull;

import com.spiraquis.alexis.imdb_test.selection.Query;
import com.spiraquis.alexis.imdb_test.movies.domain.model.Movie;

import java.util.List;

/**
 * Representación de fuente de datos local
 */

public interface ILocalMoviesDataSource {
    interface GetCallback {
        void onMoviesLoaded(List<Movie> movies);

        void onDataNotAvailable(String error);
    }

    void get(@NonNull Query query, int category, @NonNull GetCallback getCallback);

    void save(@NonNull Movie movie, int category);

    void delete(int category);
}
