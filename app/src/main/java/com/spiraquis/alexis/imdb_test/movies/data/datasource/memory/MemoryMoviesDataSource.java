package com.spiraquis.alexis.imdb_test.movies.data.datasource.memory;

import android.support.annotation.NonNull;

import com.spiraquis.alexis.imdb_test.movies.domain.criteria.MoviesSelector;
import com.spiraquis.alexis.imdb_test.selection.Query;
import com.spiraquis.alexis.imdb_test.movies.domain.model.Movie;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Objects;

/**
 * Implementación concreta de la fuente de datos en memoria
 */
public class MemoryMoviesDataSource implements IMemoryMoviesDataSource {

    private static HashMap<Integer, Movie> mCachedMovies = null;

    @Override
    public List<Movie> find(@NonNull Query query) {
        Objects.requireNonNull(query, "query no puede ser null");

        ArrayList<Movie> movies = new ArrayList<>(mCachedMovies.values());
        MoviesSelector selector = new MoviesSelector(query);
        return selector.selectListRows(movies);
    }

    @Override
    public void save(Movie movie) {
        if (mCachedMovies == null) {
            mCachedMovies = new LinkedHashMap<>();
        }
        mCachedMovies.put(movie.getmId(), movie);
    }

    @Override
    public void deleteAll() {
        if (mCachedMovies == null) {
            mCachedMovies = new LinkedHashMap<>();
        }
        mCachedMovies.clear();
    }

    @Override
    public boolean mapIsNull() {
        return mCachedMovies == null;
    }
}
