package com.spiraquis.alexis.imdb_test.external.api;

import com.spiraquis.alexis.imdb_test.movies.domain.model.TmdbApiResponse;

import java.util.Map;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.QueryMap;

/**
 * Segmentos de URL donde actuaremos
 */
public interface RestService {

    @GET("movie/popular")
    Call<TmdbApiResponse> getPopular(@QueryMap Map<String, String> params);
    @GET("movie/top_rated")
    Call<TmdbApiResponse> getTop(@QueryMap Map<String, String> params);
    @GET("movie/upcoming")
    Call<TmdbApiResponse> getUpcoming(@QueryMap Map<String, String> params);
    @GET("movie/{movie_id}")
    Call<TmdbApiResponse> getMovieDetail(@Path("movie_id") int idMovie, @QueryMap Map<String, String> params);

}
