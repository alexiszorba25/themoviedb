package com.spiraquis.alexis.imdb_test.movies.data.datasource.cloud;

import android.support.annotation.NonNull;

import com.spiraquis.alexis.imdb_test.R;
import com.spiraquis.alexis.imdb_test.application.TmdbApplication;
import com.spiraquis.alexis.imdb_test.external.api.ServiceHelper;
import com.spiraquis.alexis.imdb_test.movies.domain.model.TmdbApiResponse;
import com.spiraquis.alexis.imdb_test.selection.Query;
import com.spiraquis.alexis.imdb_test.external.api.ErrorResponse;
import com.spiraquis.alexis.imdb_test.movies.domain.model.Movie;

import java.util.List;
import java.util.Map;
import java.util.Objects;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Fuente de datos relacionada al servidor remoto
 */
public class CloudMoviesDataSource implements ICloudMoviesDataSource {

    @Override
    public void getPopular(@NonNull Query query, Map<String, String> params, final MovieServiceCallback callback) {
        Objects.requireNonNull(query, "query no puede ser null");
        ServiceHelper.getInstance().getPopular(params)
                .enqueue(new Callback<TmdbApiResponse>() {
            @Override
            public void onResponse(@NonNull Call<TmdbApiResponse> call,
                                   @NonNull Response<TmdbApiResponse> response) {
                // Procesamos los posibles casos
                processgetMoviesResponse(response, callback);

            }
            @Override
            public void onFailure(@NonNull Call<TmdbApiResponse> call, @NonNull Throwable t) {
                callback.onError(t.getMessage());
            }
        });
    }

    @Override
    public void getTop(@NonNull Query query, Map<String, String> params,
                           final MovieServiceCallback callback) {
        Objects.requireNonNull(query, "query no puede ser null");

        ServiceHelper.getInstance().getTop(params)
                .enqueue(new Callback<TmdbApiResponse>() {
                    @Override
                    public void onResponse(@NonNull Call<TmdbApiResponse> call,
                                           @NonNull Response<TmdbApiResponse> response) {
                        // Procesamos los posibles casos
                        processgetMoviesResponse(response, callback);

                    }

                    @Override
                    public void onFailure(@NonNull Call<TmdbApiResponse> call, @NonNull Throwable t) {
                        callback.onError(t.getMessage());
                    }
                });
    }

    @Override
    public void getUpcoming(@NonNull Query query, Map<String, String> params,
                           final MovieServiceCallback callback) {
        Objects.requireNonNull(query, "query no puede ser null");

        ServiceHelper.getInstance().getUpcoming(params)
                .enqueue(new Callback<TmdbApiResponse>() {
                    @Override
                    public void onResponse(@NonNull Call<TmdbApiResponse> call,
                                           @NonNull Response<TmdbApiResponse> response) {
                        // Procesamos los posibles casos
                        processgetMoviesResponse(response, callback);

                    }

                    @Override
                    public void onFailure(@NonNull Call<TmdbApiResponse> call, @NonNull Throwable t) {
                        callback.onError(t.getMessage());
                    }
                });
    }
    private void processgetMoviesResponse(Response<TmdbApiResponse> response,
                                            MovieServiceCallback callback) {
        if (response.isSuccessful()) {
            List<Movie> serverMovies = response.body().getResultsList();

            if (serverMovies.size() == 0) {
                callback.onError(TmdbApplication.getContext().getString(R.string.error_no_movies));
                return;
            }
            callback.onLoaded(serverMovies);
            return;
        }

        ResponseBody errorBody = response.errorBody();

        if (errorBody.contentType().subtype().equals("json")) {
            ErrorResponse errorResponse = ErrorResponse.fromErrorBody(errorBody);
            callback.onError(errorResponse.getMessage());
            return;
        }

        // Errores ajenos a la API
        callback.onError(response.code() + " " + response.message());
    }
}
