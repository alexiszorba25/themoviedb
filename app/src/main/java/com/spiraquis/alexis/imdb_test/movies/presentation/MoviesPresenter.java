package com.spiraquis.alexis.imdb_test.movies.presentation;

import android.widget.ImageView;

import com.spiraquis.alexis.imdb_test.movies.domain.model.Movie;
import com.spiraquis.alexis.imdb_test.selection.Query;
import com.spiraquis.alexis.imdb_test.movies.domain.criteria.AllMoviesSpecification;
import com.spiraquis.alexis.imdb_test.movies.domain.usecase.IGetMovies;

import java.util.List;
import java.util.Map;
import java.util.Objects;

import retrofit2.http.QueryMap;

/**
 * Presentador que escucha los eventos de la UI y luego presenta los resultados a la vista
 */
public class MoviesPresenter implements MoviesMvp.Presenter {

    private final MoviesMvp.View mMoviesView;
    private final IGetMovies mgetMovies;

    private static final int MOVIES_LIMIT = 20;

    private boolean mIsFirstLoad = true;
    private int mCurrentPage = 1;


    public MoviesPresenter(MoviesMvp.View moviesView, IGetMovies getMovies) {
        mMoviesView = Objects.requireNonNull(moviesView, "moviesView no puede ser null");
        mgetMovies = Objects.requireNonNull(getMovies, "GetMovies no puede ser null");
    }

    @Override
    public void loadMovies(final boolean reload, int category, @QueryMap final Map<String, String> params) {

        if (reload || mIsFirstLoad) {
            mMoviesView.showLoadingState(true);
            mCurrentPage = 1; // Reset...
        } else {
            mMoviesView.showLoadMoreIndicator(true);
            mCurrentPage++;
            params.put("page",((Integer)mCurrentPage).toString());
        }

        // Construir Query...
        Query query = new Query(
                new AllMoviesSpecification(), // Filtro
                "mTitle", Query.ASC_ORDER,        // Orden
                mCurrentPage, MOVIES_LIMIT);  // Paginado

        // Retornar Películas...
        mgetMovies.getMovies(query, reload, category, params, new IGetMovies.getMoviesCallback() {
            @Override
            public void onSuccess(List<Movie> movies) {
                mMoviesView.showLoadingState(false);
                processMovies(movies, reload||mIsFirstLoad);
                mIsFirstLoad = false;
            }

            @Override
            public void onError(String error) {
                mMoviesView.showLoadingState(false);
                mMoviesView.showLoadMoreIndicator(false);
                mMoviesView.showMoviesError(error);
            }
        });

    }

    @Override
    public void openMovieDetails(Movie movie, ImageView posterpath) {
        Objects.requireNonNull(movie, "idMovie no puede ser null");
        mMoviesView.showMovieDetailScreen(movie, posterpath);
    }

    private void processMovies(List<Movie> movies, boolean reload) {
        if (movies.isEmpty()) {
            if (reload) {
                mMoviesView.showEmptyState();
            } else {
                mMoviesView.showLoadMoreIndicator(false);
            }
            mMoviesView.allowMoreData(false);
        } else {

            if (reload) {
                mMoviesView.showMovies(movies);
            } else {
                mMoviesView.showLoadMoreIndicator(false);
                mMoviesView.showMoviesPage(movies);
            }

            mMoviesView.allowMoreData(true);
        }
    }
}
