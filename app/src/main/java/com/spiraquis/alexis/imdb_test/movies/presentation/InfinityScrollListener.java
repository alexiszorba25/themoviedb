package com.spiraquis.alexis.imdb_test.movies.presentation;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import java.util.Objects;

/**
 * {@link RecyclerView.OnScrollListener} para escrolling infinito
 */
public abstract class InfinityScrollListener extends RecyclerView.OnScrollListener {
    private static final int VISIBLE_THRESHOLD = 5;
    private final LinearLayoutManager mLayoutManager;
    private final DataLoading mDataLoading;

    InfinityScrollListener(DataLoading dataLoading, LinearLayoutManager linearLayoutManager) {
        mDataLoading = Objects.requireNonNull(dataLoading);
        mLayoutManager = Objects.requireNonNull(linearLayoutManager);
    }

    @Override
    public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
        if (dy < 0 || mDataLoading.isLoadingData() || !mDataLoading.isThereMoreData()) return;

        final int visibleItemCount = recyclerView.getChildCount();
        final int totalItemCount = mLayoutManager.getItemCount();
        final int firstVisibleItem = mLayoutManager.findFirstVisibleItemPosition();

        if ((totalItemCount - visibleItemCount) <= (firstVisibleItem + VISIBLE_THRESHOLD)) {
            onLoadMore();
        }
    }

    public abstract void onLoadMore();

}
