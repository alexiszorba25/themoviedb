package com.spiraquis.alexis.imdb_test;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;

import com.spiraquis.alexis.imdb_test.util.BottomNavigation.BottomNavigationViewBehavior;
import com.spiraquis.alexis.imdb_test.util.BottomNavigation.BottomNavigationViewHelper;

public class MainActivity extends AppCompatActivity implements BottomNavigationView.OnNavigationItemSelectedListener{
    private static final int TAB_POPULAR = R.id.navigation_popular;
    private static final int TAB_TOP = R.id.navigation_top;
    private static final int TAB_UPCOMING = R.id.navigation_upcoming;
    private MoviesMvpController mMoviesMvpController;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Crear controlador de películas populares
        mMoviesMvpController = MoviesMvpController.createMoviesMvp(this);
        setupBottomNavigation();
    }

    private void setupBottomNavigation(){

        BottomNavigationView navigation;
        navigation = findViewById(R.id.navigation);
        BottomNavigationViewHelper.disableShiftMode(navigation);
        //Seteo behavior para que se oculte
        CoordinatorLayout.LayoutParams layoutParams = (CoordinatorLayout.LayoutParams) navigation.getLayoutParams();
        layoutParams.setBehavior(new BottomNavigationViewBehavior());
        navigation.setOnNavigationItemSelectedListener(this);
        navigation.setSelectedItemId(TAB_POPULAR);
    }
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case TAB_POPULAR:
                mMoviesMvpController.initPopularMvp();
                return true;
            case TAB_TOP:
                mMoviesMvpController.initTopMvp();
                return true;
            case TAB_UPCOMING:
                mMoviesMvpController.initUpcomingMvp();
                return true;
        }
        return false;
    }
}
