package com.spiraquis.alexis.imdb_test;

import android.support.annotation.NonNull;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;

import com.spiraquis.alexis.imdb_test.di.DependencyProvider;
import com.spiraquis.alexis.imdb_test.movies.presentation.PopularFragment;
import com.spiraquis.alexis.imdb_test.movies.presentation.MoviesMvp;
import com.spiraquis.alexis.imdb_test.movies.presentation.MoviesPresenter;
import com.spiraquis.alexis.imdb_test.movies.presentation.TopFragment;
import com.spiraquis.alexis.imdb_test.movies.presentation.UpcomingFragment;

import java.util.Objects;

/**
 * Crea los fragmentos de la vista y gestiona el comportamiento de sus presentadores
 */

public class MoviesMvpController {

    private final FragmentActivity mMoviesActivity;
    private PopularFragment popularFragment;
    private TopFragment topFragment;
    private UpcomingFragment upcomingFragment;

    private MoviesMvpController(FragmentActivity moviesActivity) {
        mMoviesActivity = moviesActivity;
    }

    public static MoviesMvpController createMoviesMvp(@NonNull AppCompatActivity moviesActivity) {
        Objects.requireNonNull(moviesActivity);

        return new MoviesMvpController(moviesActivity);
    }

    public void initPopularMvp() {
        popularFragment = findOrCreatePopularFragment(R.id.frame_container);
        MoviesPresenter mPopularPresenter = createPopularPresenter(popularFragment);
        popularFragment.setPresenter(mPopularPresenter);
    }

    public void initTopMvp() {
        topFragment = findOrCreateTopFragment(R.id.frame_container);
        MoviesPresenter mTopPresenter = createTopPresenter(topFragment);
        topFragment.setPresenter(mTopPresenter);
    }
    public void initUpcomingMvp() {
        upcomingFragment = findOrCreateUpcomingFragment(R.id.frame_container);
        MoviesPresenter mUpcomingPresenter = createUpcomingPresenter(upcomingFragment);
        upcomingFragment.setPresenter(mUpcomingPresenter);
    }
    private PopularFragment findOrCreatePopularFragment(int container) {
        if (popularFragment == null) {
            popularFragment = PopularFragment.newInstance();
        }
        getSupportFragmentManager()
                .beginTransaction()
                .replace(container, popularFragment)
                .setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out)
                .commit();
        return popularFragment;
    }
    private TopFragment findOrCreateTopFragment(int container) {
        if (topFragment == null) {
            topFragment = TopFragment.newInstance();
        }
        getSupportFragmentManager()
                .beginTransaction()
                .replace(container, topFragment)
                .setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out)
                .commit();
        return topFragment;
    }
    private UpcomingFragment findOrCreateUpcomingFragment(int container) {
        if (upcomingFragment == null) {
            upcomingFragment = UpcomingFragment.newInstance();
        }
        getSupportFragmentManager()
                .beginTransaction()
                .replace(container, upcomingFragment)
                .setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out)
                .commit();
        return upcomingFragment;
    }
    private MoviesPresenter createPopularPresenter(MoviesMvp.View moviesView) {
        return new MoviesPresenter(
                moviesView,
                DependencyProvider.providegetPopular(mMoviesActivity));

    }
    private MoviesPresenter createTopPresenter(MoviesMvp.View moviesView) {
        return new MoviesPresenter(
                moviesView,
                DependencyProvider.providegetTop(mMoviesActivity));

    }
    private MoviesPresenter createUpcomingPresenter(MoviesMvp.View moviesView) {
        return new MoviesPresenter(
                moviesView,
                DependencyProvider.providegetUpcoming(mMoviesActivity));

    }

    private FragmentManager getSupportFragmentManager() {
        return mMoviesActivity.getSupportFragmentManager();
    }

}
