package com.spiraquis.alexis.imdb_test.moviedetail;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import com.spiraquis.alexis.imdb_test.R;
import com.spiraquis.alexis.imdb_test.moviedetail.presentation.MovieDetailFragment;
import com.spiraquis.alexis.imdb_test.moviedetail.presentation.MovieDetailPresenter;
import com.spiraquis.alexis.imdb_test.movies.domain.model.Movie;

public class MovieDetailActivity extends AppCompatActivity {

    public static final String EXTRA_ID_MOVIE = "extra.id_movie";
    public static final String EXTRA_MOVIE = "extra.movie";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_movie_detail);

        // Crear fragmento
        MovieDetailFragment fragment = (MovieDetailFragment) getSupportFragmentManager()
                .findFragmentById(R.id.content_movie_detail_container);
        if (fragment == null) {
            fragment = MovieDetailFragment.newInstance();

            getSupportFragmentManager().beginTransaction()
                    .add(R.id.content_movie_detail_container, fragment)
                    .commit();
        }

        Movie movie = getIntent().getParcelableExtra(EXTRA_MOVIE);
        MovieDetailPresenter detailPresenter = new MovieDetailPresenter(movie, fragment);
        fragment.setPresenter(detailPresenter);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
