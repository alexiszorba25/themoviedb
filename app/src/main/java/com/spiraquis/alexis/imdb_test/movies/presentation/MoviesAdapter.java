package com.spiraquis.alexis.imdb_test.movies.presentation;

import android.content.Context;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.spiraquis.alexis.imdb_test.R;
import com.spiraquis.alexis.imdb_test.movies.domain.model.Movie;
import com.spiraquis.alexis.imdb_test.util.DateUtils;
import com.spiraquis.alexis.imdb_test.util.GlideApp;

import java.util.List;
import java.util.Objects;

import static com.spiraquis.alexis.imdb_test.application.TmdbApplication.getContext;

/**
 * Adaptador de películas
 */
public class MoviesAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>
        implements DataLoading {

    private List<Movie> mMovies;
    private MovieItemListener mItemListener;

    private final static int TYPE_MOVIE = 1;
    private final static int TYPE_LOADING_MORE = 2;

    private boolean mLoading = false;
    private boolean mMoreData = false;


    MoviesAdapter(List<Movie> movies, MovieItemListener itemListener) {
        setList(movies);
        mItemListener = itemListener;
    }

    @Override
    public int getItemViewType(int position) {
        if (position < getDataItemCount() && getDataItemCount() > 0) {
            return TYPE_MOVIE;
        }
        return TYPE_LOADING_MORE;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);
        View view;

        if (viewType == TYPE_LOADING_MORE) {
            view = inflater.inflate(R.layout.item_loading, parent, false);
            return new LoadingMoreHolder(view);
        }

        view = inflater.inflate(R.layout.item_movie, parent, false);
        return new MoviesHolder(view, mItemListener);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int position) {
        switch (getItemViewType(position)) {
            case TYPE_MOVIE:
                Movie movie = mMovies.get(position);
                MoviesHolder moviesHolder = (MoviesHolder) viewHolder;
                ViewCompat.setTransitionName(moviesHolder.poster_path, movie.getmId().toString());
                moviesHolder.movieTitle.setText(getContext().getString(R.string.txt_year,String.valueOf(movie.getmTitle()), DateUtils.getYearFromDate(movie.getmReleaseDate())));
                moviesHolder.vote_average.setText(String.valueOf(movie.getmVoteAverage()));
                GlideApp.with(viewHolder.itemView.getContext())
                        .load(getContext().getString(R.string.base_url_poster)+movie.getmPosterPath())
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .fitCenter()
                        .into(moviesHolder.poster_path);
                break;
            case TYPE_LOADING_MORE:
                bindLoadingViewHolder((LoadingMoreHolder) viewHolder, position);
                break;
        }

    }

    private void bindLoadingViewHolder(LoadingMoreHolder viewHolder, int position) {
        viewHolder.progress.setVisibility((position > 0 && mLoading && mMoreData)
                ? View.VISIBLE : View.INVISIBLE);
    }

    public void replaceData(List<Movie> movies) {
        setList(movies);
        notifyDataSetChanged();
    }

    private void setList(List<Movie> movies) {
        if (mMovies!=null) {
            mMovies.clear();
            mMovies.addAll(Objects.requireNonNull(movies));
        }else{
            mMovies = movies;
        }
    }

    public void addData(List<Movie> movies) {
        mMovies.addAll(movies);
    }

    @Override
    public int getItemCount() {
        return getDataItemCount() + (mLoading ? 1 : 0);
    }

    private Movie getItem(int position) {
        return mMovies.get(position);
    }


    public void dataStartedLoading() {
        if (mLoading) return;
        mLoading = true;
        Handler h = new Handler();
        h.post(new Runnable() {
            @Override
            public void run() {
                notifyItemInserted(getLoadingMoreItemPosition());
            }
        });

    }

    public void dataFinishedLoading() {
        if (!mLoading) return;
        final int loadingPos = getLoadingMoreItemPosition();
        mLoading = false;
        Handler h = new Handler();
        h.post(new Runnable() {
            @Override
            public void run() {
                notifyItemRemoved(loadingPos);
            }
        });

    }

    public void setMoreData(boolean more) {
        mMoreData = more;
    }


    private int getLoadingMoreItemPosition() {
        return mLoading ? getItemCount() - 1 : RecyclerView.NO_POSITION;
    }

    public int getDataItemCount() {
        return mMovies.size();
    }


    @Override
    public boolean isLoadingData() {
        return mLoading;
    }

    @Override
    public boolean isThereMoreData() {
        return mMoreData;
    }

    public class MoviesHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView vote_average;
        ImageView poster_path;
        TextView movieTitle;

        private MovieItemListener mItemListener;

        MoviesHolder(View itemView, MovieItemListener listener) {
            super(itemView);
            mItemListener = listener;
            vote_average = itemView.findViewById(R.id.vote_average);
            poster_path = itemView.findViewById(R.id.posterpath);
            movieTitle = itemView.findViewById(R.id.movieTitle);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            int position = getAdapterPosition();
            Movie movie = getItem(position);
            mItemListener.onMovieClick(movie, poster_path);

        }
    }

    private class LoadingMoreHolder extends RecyclerView.ViewHolder {
        ProgressBar progress;

        LoadingMoreHolder(View view) {
            super(view);
            progress = view.findViewById(R.id.progressBar);
        }
    }

    public interface MovieItemListener {
        void onMovieClick(Movie clickedNote, ImageView posterpath);

    }

}
