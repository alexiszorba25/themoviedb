package com.spiraquis.alexis.imdb_test.movies.presentation;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.spiraquis.alexis.imdb_test.R;
import com.spiraquis.alexis.imdb_test.moviedetail.MovieDetailActivity;
import com.spiraquis.alexis.imdb_test.movies.domain.criteria.MoviesSelector;
import com.spiraquis.alexis.imdb_test.movies.domain.model.Movie;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;

import static com.spiraquis.alexis.imdb_test.application.TmdbApplication.UPCOMING;

/**
 * Fragmento para mostrar la lista de películas Top
 */
public class UpcomingFragment extends Fragment implements MoviesMvp.View {

    // Referencias UI
    private RecyclerView mMoviesList;
    private MoviesAdapter mUpcomingAdapter;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private View mEmptyView;
    private MoviesAdapter.MovieItemListener mItemListener =
            new MoviesAdapter.MovieItemListener() {
                @Override
                public void onMovieClick(Movie clickedMovie, ImageView posterpath) {
                    mMoviesPresenter.openMovieDetails(clickedMovie, posterpath);
                }
            };
    LinearLayoutManager layoutManager;
    // Relaciones de composición
    private MoviesMvp.Presenter mMoviesPresenter;

    private List<Movie> movieList = new ArrayList<>();

    public UpcomingFragment() {
        // Required empty public constructor
    }

    public static UpcomingFragment newInstance() {
        return new UpcomingFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mUpcomingAdapter = new MoviesAdapter(new ArrayList<Movie>(0), mItemListener);

        //setRetainInstance(true);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_upcoming, container, false);

        mMoviesList = root.findViewById(R.id.upcoming_list);
        mEmptyView = root.findViewById(R.id.noMovies);
        mSwipeRefreshLayout = root.findViewById(R.id.refresh_layout);
        setToolbar(root);
        layoutManager = (LinearLayoutManager) mMoviesList.getLayoutManager();
        setUpMoviesList();
        setUptRefreshLayout();

        if (savedInstanceState != null) {
            hideList(false);
        }

        return root;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_search, menu);
        final MenuItem mSearch = menu.findItem(R.id.action_search);
        final SearchView mSearchView = (SearchView) mSearch.getActionView();
        mSearchView.setQueryHint(getString(R.string.txt_search));
        mSearchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                setScrollListener(false);
                MoviesSelector selector = new MoviesSelector(null);
                searchMovies(selector.filterListRows(movieList,newText));
                return true;
            }
        });
        mSearch.setOnActionExpandListener( new MenuItem.OnActionExpandListener() {
            @Override
            public boolean onMenuItemActionExpand(MenuItem item) {
                return true;
            }
            @Override
            public boolean onMenuItemActionCollapse(MenuItem item) {
                setScrollListener(true);
                showMovies(movieList);
                return true;

            }
        });
        super.onCreateOptionsMenu(menu, inflater);
    }
    private void setToolbar(View root) {
        Toolbar toolbar = root.findViewById(R.id.toolbar);
        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);
        ActionBar ab = ((AppCompatActivity) getActivity()).getSupportActionBar();
        ab.setTitle(getString(R.string.category_upcoming));
    }
    @Override
    public void onResume() {
        super.onResume();
        mMoviesPresenter.loadMovies(true, UPCOMING, setWsParams());
    }
    private Map<String, String> setWsParams() {
        Map<String, String> wsParams = new HashMap<>();
        wsParams.put("api_key", getActivity().getString(R.string.api_key));
        wsParams.put("language", Locale.getDefault().getLanguage());
        return wsParams;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return false;
    }

    private void setUpMoviesList() {
        mMoviesList.setAdapter(mUpcomingAdapter);

        setScrollListener(true);
    }
    private void setScrollListener(boolean enable) {
        if (enable) {
            mMoviesList.addOnScrollListener(
                    new InfinityScrollListener(mUpcomingAdapter, layoutManager) {
                        @Override
                        public void onLoadMore() {
                            mMoviesPresenter.loadMovies(false, UPCOMING, setWsParams());
                        }
                    });
        }else{
            mMoviesList.clearOnScrollListeners();
        }
    }
    private void setUptRefreshLayout() {
        mSwipeRefreshLayout.setColorSchemeColors(
                ContextCompat.getColor(getActivity(), R.color.colorPrimary),
                ContextCompat.getColor(getActivity(), R.color.colorAccent),
                ContextCompat.getColor(getActivity(), R.color.colorPrimaryDark));

        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                mMoviesPresenter.loadMovies(true, UPCOMING, setWsParams());
            }
        });
    }

    private void searchMovies(List<Movie> movies) {
        mUpcomingAdapter.replaceData(movies);
    }
    @Override
    public void showMovies(List<Movie> movies) {
        mUpcomingAdapter.replaceData(movies);
        movieList = movies;
        hideList(false);
    }

    @Override
    public void showMoviesPage(List<Movie> movies) {
        mUpcomingAdapter.addData(movies);
        movieList.addAll(movies);
        hideList(false);
    }

    @Override
    public void showLoadingState(final boolean show) {
        if (getView() == null) {
            return;
        }

        mSwipeRefreshLayout.post(new Runnable() {
            @Override
            public void run() {
                mSwipeRefreshLayout.setRefreshing(show);
            }
        });
    }

    @Override
    public void showEmptyState() {
        if (getView() == null) {
            return;
        }
        hideList(true);
    }

    private void hideList(boolean hide) {
        mMoviesList.setVisibility(hide ? View.GONE : View.VISIBLE);
        mEmptyView.setVisibility(hide ? View.VISIBLE : View.GONE);
    }

    @Override
    public void showMoviesError(String msg) {
        hideList(true);
        Toast.makeText(getActivity(), msg, Toast.LENGTH_LONG)
                .show();
    }

    @Override
    public void showLoadMoreIndicator(boolean show) {
        if (!show) {
            mUpcomingAdapter.dataFinishedLoading();
        } else {
            mUpcomingAdapter.dataStartedLoading();
        }
    }

    @Override
    public void allowMoreData(boolean allow) {
        mUpcomingAdapter.setMoreData(allow);
    }

    @Override
    public void showMovieDetailScreen(Movie movie, ImageView posterpath) {
        Intent i = new Intent(getActivity(), MovieDetailActivity.class);
        i.putExtra(MovieDetailActivity.EXTRA_ID_MOVIE, movie.getmId());
        i.putExtra(MovieDetailActivity.EXTRA_MOVIE, movie);
        i.putExtra("transition", ViewCompat.getTransitionName(posterpath));
        ActivityOptionsCompat options = ActivityOptionsCompat.
                makeSceneTransitionAnimation(getActivity(), posterpath, ViewCompat.getTransitionName(posterpath));
        startActivity(i, options.toBundle());
    }

    @Override
    public void setPresenter(MoviesMvp.Presenter presenter) {
        mMoviesPresenter = Objects.requireNonNull(presenter);
    }

    @Override
    public MoviesMvp.Presenter getPresenter() {
        return mMoviesPresenter;
    }
}
