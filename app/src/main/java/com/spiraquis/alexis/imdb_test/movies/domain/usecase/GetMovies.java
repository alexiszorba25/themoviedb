package com.spiraquis.alexis.imdb_test.movies.domain.usecase;

import android.support.annotation.NonNull;

import com.spiraquis.alexis.imdb_test.selection.Query;
import com.spiraquis.alexis.imdb_test.movies.data.IMoviesRepository;
import com.spiraquis.alexis.imdb_test.movies.domain.model.Movie;

import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * Implementación concreta del interactor de detalle de película
 */

public class GetMovies implements IGetMovies {
    private IMoviesRepository mMoviesRepository;


    public GetMovies(IMoviesRepository moviesRepository) {
        mMoviesRepository = Objects.requireNonNull(moviesRepository,
                "moviesRepository no puede ser null");
    }

    @Override
    public void getMovies(@NonNull final Query query, boolean forceLoad, int category, Map<String, String> params,
                            final getMoviesCallback callback) {
        Objects.requireNonNull(query, "query no puede ser null");
        Objects.requireNonNull(callback, "callback no puede ser null");


        if (forceLoad) {
            mMoviesRepository.refreshMovies();
        }

        mMoviesRepository.getMovies(query, category, params, new IMoviesRepository.getMoviesCallback() {
                    @Override
                    public void onMoviesLoaded(List<Movie> movies) {
                        Objects.requireNonNull(movies, "movies no puede ser null");
                        callback.onSuccess(movies);
                    }

                    @Override
                    public void onDataNotAvailable(String error) {
                        callback.onError(error);
                    }
                }
        );
    }
}
