package com.spiraquis.alexis.imdb_test.movies.data.datasource.local;

import android.content.ContentResolver;
import android.support.annotation.NonNull;

import com.spiraquis.alexis.imdb_test.selection.Query;
import com.spiraquis.alexis.imdb_test.movies.domain.model.Movie;
import com.spiraquis.alexis.imdb_test.util.LocalDbUtils;

import java.util.Objects;

/**
 * Implementación concreta de la base local Sqlite
 */

public class LocalMoviesDataSource implements ILocalMoviesDataSource {

    public LocalMoviesDataSource(@NonNull ContentResolver contentResolver) {
        ContentResolver mContentResolver = Objects.requireNonNull(contentResolver);
    }

    @Override
    public void get(@NonNull Query query, int category, @NonNull final GetCallback getCallback) {
        Objects.requireNonNull(query, "query no puede ser null");
        try{
            getCallback.onMoviesLoaded(LocalDbUtils.getMoviesByCategory(category));
        }catch (Exception e) {
            getCallback.onDataNotAvailable(e.getMessage());
        }
    }

    @Override
    public void save(@NonNull Movie movie, int category) {
        LocalDbUtils.saveMovie(movie, category);
    }

    @Override
    public void delete(int category) {
        LocalDbUtils.deleteMoviesByCategory(category);
    }

}
