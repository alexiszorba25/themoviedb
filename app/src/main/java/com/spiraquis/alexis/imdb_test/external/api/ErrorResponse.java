package com.spiraquis.alexis.imdb_test.external.api;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;

import java.io.IOException;

import okhttp3.ResponseBody;

/**
 * Entidad para recibir respuestas de error de la API
 */
public class ErrorResponse {
    @SerializedName("message")
    private String mMessage;

    public String getMessage() {
        return mMessage;
    }

    public static ErrorResponse fromErrorBody(ResponseBody errorBody) {
        try {

            return new Gson()
                    .fromJson(errorBody.string(), ErrorResponse.class);

        } catch (IOException e) {
            e.printStackTrace();
        }

        return new ErrorResponse();
    }
}
