package com.spiraquis.alexis.imdb_test.util;

import com.spiraquis.alexis.imdb_test.movies.domain.model.LocalMovie;
import com.spiraquis.alexis.imdb_test.movies.domain.model.Movie;

import java.util.ArrayList;
import java.util.List;

import nl.qbusict.cupboard.QueryResultIterable;

import static com.spiraquis.alexis.imdb_test.application.TmdbApplication.db;
import static nl.qbusict.cupboard.CupboardFactory.cupboard;

public class LocalDbUtils {
    public static void saveMovie(Movie movie, int category){
        LocalMovie movieLocal =
                new LocalMovie(
                        movie.getmId(),
                        category,
                        movie.getmTitle(),
                        movie.getmOrigLanguage(),
                        movie.getmPosterPath(),
                        movie.getmOverview(),
                        movie.getmReleaseDate(),
                        movie.getmBackdropPath(),
                        movie.getmVoteCount(),
                        movie.getmVoteAverage()
                );
        cupboard().withDatabase(db).put(movieLocal);
    }
    public static void deleteMoviesByCategory(Integer category) {
        cupboard().withDatabase(db).delete(LocalMovie.class, "mCategory = ?", category.toString());
    }
    public static List<Movie> getMoviesByCategory(Integer category) {
        QueryResultIterable<LocalMovie> itr;
        List<Movie> movieList = new ArrayList<>();
        itr =  cupboard().withDatabase(db).query(LocalMovie.class).withSelection( "mCategory = ?", category.toString()).orderBy("mVoteAverage desc").query();
        for (LocalMovie localMovie: itr) {
            movieList.add(getMovieFromLocalMovie(localMovie));
        }
        return movieList;
    }
    private static Movie getMovieFromLocalMovie(LocalMovie localMovie) {
        return new Movie(
                localMovie.getmId(),
                localMovie.getmTitle(),
                localMovie.getmOrigLanguage(),
                localMovie.getmPosterPath(),
                localMovie.getmOverview(),
                localMovie.getmReleaseDate(),
                localMovie.getmBackdropPath(),
                localMovie.getmVoteCount(),
                localMovie.getmVoteAverage());
    }
}
