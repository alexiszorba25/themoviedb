package com.spiraquis.alexis.imdb_test.movies.data.datasource.memory;

import android.support.annotation.NonNull;

import com.spiraquis.alexis.imdb_test.selection.Query;
import com.spiraquis.alexis.imdb_test.movies.domain.model.Movie;

import java.util.List;

/**
 * Interfaz para fuente de datos en memoria
 */
public interface IMemoryMoviesDataSource {
    List<Movie> find(@NonNull Query query);

    void save(Movie movie);

    void deleteAll();

    boolean mapIsNull();
}
