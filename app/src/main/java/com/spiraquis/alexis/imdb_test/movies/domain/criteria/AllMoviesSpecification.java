package com.spiraquis.alexis.imdb_test.movies.domain.criteria;

import com.spiraquis.alexis.imdb_test.selection.specification.MemorySpecification;
import com.spiraquis.alexis.imdb_test.movies.domain.model.Movie;

/**
 * Criterio para obtener todas las películas
 */

public class AllMoviesSpecification
        implements MemorySpecification<Movie>{
    @Override
    public boolean isSatisfiedBy(Movie item) {
        return true;
    }

}
