package com.spiraquis.alexis.imdb_test.movies.domain.criteria;

import com.spiraquis.alexis.imdb_test.movies.domain.model.Movie;
import com.spiraquis.alexis.imdb_test.selection.Query;
import com.spiraquis.alexis.imdb_test.selection.selector.FilterSelector;
import com.spiraquis.alexis.imdb_test.selection.selector.ListSelector;
import com.spiraquis.alexis.imdb_test.selection.specification.MemorySpecification;
import com.spiraquis.alexis.imdb_test.util.CollectionsUtils;

import java.util.ArrayList;
import java.util.List;

public class MoviesSelector implements
        ListSelector<Movie>, FilterSelector<Movie, String> {

    private final Query mQuery;

    public MoviesSelector(Query query) {
        mQuery = query;
    }

    @Override
    public List<Movie> selectListRows(List<Movie> movies) {
        List<Movie> affectedMovies = new ArrayList<>(movies);

        // Elegir Página...
        affectedMovies = CollectionsUtils.getPage(affectedMovies,
                mQuery.getPageNumber(), mQuery.getPageSize());

        return affectedMovies;
    }

    @Override
    public List<Movie> filterListRows(List<Movie> movies, String filter) {
        filter = filter.toLowerCase();
        final List<Movie> filteredMovieList = new ArrayList<>();
        for (Movie movie : movies) {
            final String text = movie.getmTitle().toLowerCase();
            if (text.contains(filter)) {
                filteredMovieList.add(movie);
            }
        }
        return filteredMovieList;
    }
}
