package com.spiraquis.alexis.imdb_test.movies.data.datasource.cloud;

import android.support.annotation.NonNull;

import com.spiraquis.alexis.imdb_test.movies.domain.model.Movie;
import com.spiraquis.alexis.imdb_test.selection.Query;

import java.util.List;
import java.util.Map;

/**
 * Interfaz de comunicación con el repositorio para la fuente de datos remota
 */
public interface ICloudMoviesDataSource {

    interface MovieServiceCallback {

        void onLoaded(List<Movie> movies);

        void onError(String error);

    }

    void getPopular(@NonNull Query query, Map<String, String> params,  MovieServiceCallback callback);
    void getTop(@NonNull Query query, Map<String, String> params, MovieServiceCallback callback);
    void getUpcoming(@NonNull Query query, Map<String, String> params, MovieServiceCallback callback);

}
