package com.spiraquis.alexis.imdb_test.movies.data;

import android.support.annotation.NonNull;

import com.spiraquis.alexis.imdb_test.selection.Query;
import com.spiraquis.alexis.imdb_test.movies.domain.model.Movie;

import java.util.List;
import java.util.Map;

/**
 * Repositorio de películas
 */
public interface IMoviesRepository {
    interface getMoviesCallback {

        void onMoviesLoaded(List<Movie> movies);

        void onDataNotAvailable(String error);
    }

    void getMovies(@NonNull Query query, int category, Map<String, String> params, getMoviesCallback callback);

    void refreshMovies();
}
