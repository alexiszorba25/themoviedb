package com.spiraquis.alexis.imdb_test.external.api;

import com.google.gson.Gson;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.spiraquis.alexis.imdb_test.movies.domain.model.TmdbApiResponse;

import java.lang.reflect.Type;

public class TmdbDeserializer implements JsonDeserializer<TmdbApiResponse>
{
        @Override
        public TmdbApiResponse deserialize(JsonElement je, Type type, JsonDeserializationContext jdc)
                throws JsonParseException
        {
            JsonElement results = je.getAsJsonObject().get("results");

            return new Gson().fromJson(results, TmdbApiResponse.class);

        }
}
