package com.spiraquis.alexis.imdb_test.movies.domain.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;
import com.spiraquis.alexis.imdb_test.util.DateUtils;

import java.util.Locale;
import java.util.UUID;

/**
 * Entidad de negocio para las películas
 */
public class Movie implements Parcelable{
    @SerializedName("id")
    private Integer mId;

    @SerializedName("title")
    private String mTitle;

    @SerializedName("original_language")
    private String mOrigLanguage;

    @SerializedName("poster_path")
    private String mPosterPath;

    @SerializedName("overview")
    private String mOverview;

    @SerializedName("release_date")
    private String mReleaseDate;

    @SerializedName("backdrop_path")
    private String mBackdropPath;

    @SerializedName("vote_count")
    private int mVoteCount;

    @SerializedName("vote_average")
    private float mVoteAverage;

    public Movie(Integer mId, String mTitle, String mOrigLanguage, String mPosterPath, String mOverview, String mReleaseDate, String mBackdropPath, int mVoteCount, float mVoteAverage) {
        this.mId = mId;
        this.mTitle = mTitle;
        this.mOrigLanguage = mOrigLanguage;
        this.mPosterPath = mPosterPath;
        this.mOverview = mOverview;
        this.mReleaseDate = mReleaseDate;
        this.mBackdropPath = mBackdropPath;
        this.mVoteCount = mVoteCount;
        this.mVoteAverage = mVoteAverage;
    }

    protected Movie(Parcel in) {
        if (in.readByte() == 0) {
            mId = null;
        } else {
            mId = in.readInt();
        }
        mTitle = in.readString();
        mOrigLanguage = in.readString();
        mPosterPath = in.readString();
        mOverview = in.readString();
        mReleaseDate = in.readString();
        mBackdropPath = in.readString();
        mVoteCount = in.readInt();
        mVoteAverage = in.readFloat();
    }

    public static final Creator<Movie> CREATOR = new Creator<Movie>() {
        @Override
        public Movie createFromParcel(Parcel in) {
            return new Movie(in);
        }

        @Override
        public Movie[] newArray(int size) {
            return new Movie[size];
        }
    };

    public Integer getmId() {
        return mId;
    }

    public String getmTitle() {
        return mTitle;
    }

    public String getmOrigLanguage() {
        return mOrigLanguage;
    }

    public String getmPosterPath() {
        return mPosterPath;
    }

    public String getmOverview() {
        return mOverview;
    }

    public String getmReleaseDate() {
        return mReleaseDate;
    }
    public String getmBackdropPath() {
        return mBackdropPath;
    }

    public int getmVoteCount() {
        return mVoteCount;
    }

    public float getmVoteAverage() {
        return mVoteAverage;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        if (mId == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(mId);
        }
        dest.writeString(mTitle);
        dest.writeString(mOrigLanguage);
        dest.writeString(mPosterPath);
        dest.writeString(mOverview);
        dest.writeString(mReleaseDate);
        dest.writeString(mBackdropPath);
        dest.writeInt(mVoteCount);
        dest.writeFloat(mVoteAverage);
    }
}
