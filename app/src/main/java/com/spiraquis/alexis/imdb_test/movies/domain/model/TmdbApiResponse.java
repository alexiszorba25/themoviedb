package com.spiraquis.alexis.imdb_test.movies.domain.model;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;
import java.util.List;
import java.lang.reflect.Type;

public class TmdbApiResponse {
    @SerializedName("total_pages")
    private Integer totalPages;

    @SerializedName("total_results")
    private Integer totalResults;

    @SerializedName("page")
    private Integer page;

    @SerializedName("results")
    @Expose
    private JsonElement results;

    public Integer getTotalPages() {
        return totalPages;
    }

    public Integer getTotalResults() {
        return totalResults;
    }

    public Integer getPage() {
        return page;
    }

    private JsonElement getResults() {
        return results;
    }
    public List<Movie> getResultsList() {
        List<Movie> resultList = new ArrayList<>();
        Gson gson = new Gson();
        if (getResults() instanceof JsonObject) {
            resultList.add(gson.fromJson(getResults(), Movie.class));
        } else if (getResults() instanceof JsonArray) {
            Type founderListType = new TypeToken<ArrayList<Movie>>() {
            }.getType();
            resultList = gson.fromJson(getResults(), founderListType);
        }
        return resultList;
    }
}
