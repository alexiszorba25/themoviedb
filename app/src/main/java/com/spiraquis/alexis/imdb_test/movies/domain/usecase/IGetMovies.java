package com.spiraquis.alexis.imdb_test.movies.domain.usecase;

import android.support.annotation.NonNull;

import com.spiraquis.alexis.imdb_test.selection.Query;
import com.spiraquis.alexis.imdb_test.movies.domain.model.Movie;

import java.util.List;
import java.util.Map;

/**
 * Abstracción del interactor para obtener películas
 */

public interface IGetMovies {
    void getMovies(@NonNull Query query, boolean forceLoad, int category, Map<String, String> params, getMoviesCallback callback);

    interface getMoviesCallback {
        void onSuccess(List<Movie> movies);

        void onError(String error);
    }
}
