package com.spiraquis.alexis.imdb_test.util;

import com.bumptech.glide.annotation.GlideModule;
import com.bumptech.glide.module.AppGlideModule;

@GlideModule
public final class ImdbTestGlideModule extends AppGlideModule {}
